import Cookies from "js-cookie"
import axios from "axios"
import store from "./store"
import router from "./router"

export default function (Vue) {
  Vue.auth = {
    async refreshToken() {
      if (this.getToken()) {
        await axios.post("token/refresh")
            .then(response => {
              let token = response.data.access_token;
              this.setToken(token);
              axios.defaults.headers.common["Authorization"] = "Bearer " + token;
              store.commit("passAuthData", this.getAuthData());
            }).catch(() => {
              this.destroyUserData();
              router.push({name: "Login"});
            })
      } else {
        this.destroyUserData();
      }
    },

    refreshData() {
      if (this.getAuthData()) {
        store.commit("passAuthData", this.getAuthData());
      }
    },

    setToken(token) {

      Cookies.set("token", token);
    },

    getToken() {
      return Cookies.get("token");
    },

    destroyUserData() {
      Cookies.remove("token");
      Cookies.remove("auth_data");
      store.commit("passAuthData", null);
    },

    checkIfAuthTokenExists() {
      return this.getToken();
    },

    async fetchAuthData() {
      await axios.get("me")
          .then(response => {
            let userData = response.data;
            userData["user_role"] = response.data.user_role.role_id;
            this.setAuthData(userData);
          }).catch(() => {
            this.destroyUserData();
          })
    },

    setAuthData(authData) {
      Cookies.set("auth_data", JSON.stringify(authData), "1d");
      store.commit("passAuthData", authData);
    },

    getAuthData() {
      if (Cookies.get("auth_data")) {
        return JSON.parse(Cookies.get("auth_data"));
      }
    },

    getUserRole() {
      if (Cookies.get("auth_data")) {
        return Cookies.get("auth_data") ? JSON.parse(Cookies.get("auth_data")).user_role : null;
      }
    }
  };

  Object.defineProperties(Vue.prototype, {
    $auth: {
      get: () => {
        return Vue.auth
      }
    }
  })
}
