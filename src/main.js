import Vue from 'vue'
import App from './App.vue'
import vuetify from './plugins/vuetify';
import router from "./router"
import axios from 'axios'
import Vuex from "vuex"
import auth from "./auth.js"
import store from "./store"
import cookie from "js-cookie"
import Message from 'vue-m-message'
import moment from 'moment'
import VueParticles from 'vue-particles'
import 'leaflet/dist/leaflet.css';

Vue.use(Vuex);
Vue.use(auth);
Vue.use(cookie);
Vue.use(Message)
Vue.use(VueParticles)
moment.locale('pl')

Vue.prototype.moment = moment
Vue.config.productionTip = false
Vue.prototype.$http = axios;
axios.defaults.baseURL = "https://backend.ostrzezennik.pl/api/";
axios.defaults.headers.common["Authorization"] = "Bearer " + Vue.auth.getToken();

const UNAUTHORIZED = 401;
axios.interceptors.response.use(
    response => response,
    error => {
      const {status} = error.response;
      if (status === UNAUTHORIZED) {
        Vue.auth.refreshToken();
      }
      return Promise.reject(error);
    }
);

const ADMIN = 2;

router.beforeEach(
    (to, from, next) => {
      if (to.matched.some(record => record.meta.forVisitors)) {
        if (Vue.auth.getUserRole()) {
          next({
            path: "/"
          })
        } else next();
      }

      else if (to.matched.some(record => record.meta.forAdmins)) {
        if (Vue.auth.getUserRole() > ADMIN) {
          next({
            path: "/"
          })
        } else next();
      }
      else next();
    }
);

Vue.config.productionTip = false;

new Vue({
  vuetify,
  router,
  store,
  render: h => h(App)
}).$mount('#app')
