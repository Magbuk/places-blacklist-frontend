import Vue from "vue"
import Router from "vue-router"
import LandingPage from "../components/LandingPage";
import GreetingPage from "../components/GreetingPage";
import Login from "../components/Login";
import Registration from "../components/Registration";
import Entry from "../components/Entry";
import AddEntry from "../components/AddEntry";
import AdminPanel from "../components/AdminPanel";
import SearchEntries from "../components/SearchEntries";
import ActivateAccount from "../components/ActivateAccount";

Vue.use(Router);

export default new Router({
  mode: "history",
  routes: [
    {
      path: "/",
      name: "Greeting",
      component: GreetingPage,
    },
    {
      path: "/home",
      name: "Home",
      component: LandingPage,
    },
    {
      path: "/login",
      name: "Login",
      component: Login,
      meta: {
        forVisitors: true
      }
    },
    {
      path: "/register",
      name: "Registration",
      component: Registration,
      meta: {
        forVisitors: true
      }
    },
    {
      path: "/activate_account/:token",
      name: "ActivateAccount",
      component: ActivateAccount,
      meta: {
        forVisitors: true
      }
    },
    {
      path: "/entry/:id",
      name: "Entry",
      component: Entry,
    },
    {
      path: "/add-entry/",
      name: "AddEntry",
      component: AddEntry,
    },
    {
      path: "/search/",
      name: "SearchEntries",
      component: SearchEntries,
    },
    {
      path: "/admin/",
      name: "AdminPanel",
      component: AdminPanel,
      meta: {
        forAdmins: true
      }
    },
    {
      path: "*",
      redirect: "/",
      component: LandingPage
    }
  ]
})
