import Vue from "vue"
import Vuex from "vuex"

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    authData: null
  },

  mutations: {
    passAuthData(state, authData) {
      state.authData = authData
    }
  },

  getters: {
    passAuthData: state => {
      return state.authData
    },
  },
})
